# # Use Node.js as the base image
# FROM node:latest

# # Set the working directory inside the container
# WORKDIR /usr/src/app

# # Copy package.json and package-lock.json to the working directory
# COPY package*.json ./

# # Install dependencies
# RUN npm install

# # Copy the rest of the application code to the working directory
# #COPY . .
# COPY ./apps/my-workspace .

# # Expose the port your app runs on
# EXPOSE 3000

# # Command to run your application
# #CMD ["npm", "run", "start:prod"]
# CMD ["npm", "run", "start"]


#FROM node:17-alpine3.12
FROM node:18.16-alpine

WORKDIR /app

COPY . .

# Set npm cache directory
#ENV NPM_CONFIG_CACHE=/tmp/npm-cache

#RUN npm cache clean --force

#RUN npm cache clean --force \
    #&& rm -rf /root/.npm/_logs

#RUN npm install -g npm@latest
RUN npm install
#RUN npm add --global nx@latest
# RUN npm install -g nx@latest

RUN npm add -D nx@latest
RUN npm add -D @nx/nest

# RUN sudo chown -R $USER:$USER /root/.npm
# RUN sudo chown -R 1000:1000 "/root/.npm"

# RUN chown -R root app
#RUN chmod +x runjs

# Expose the port your app runs on
#EXPOSE 3000

# Specify a non-root user to run the container
#USER node

#CMD ["npm", "run", "start", "app-1"]
CMD ["npm", "run", "app1"]
#CMD ["npx", "run", "app1"]
#CMD ["npx", "nx", "serve", "my-workspace"]
